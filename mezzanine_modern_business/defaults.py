from django.utils.translation import ugettext as _
from mezzanine.conf import register_setting

register_setting(
    name="MODERN_BUSINESS_LOGO",
    description=_("Url logo. You can also upload the image to the media "
                  "library and paste the url generated here. (342x84)"),
    editable=True,
    default='',
)